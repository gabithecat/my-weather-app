import { Component } from '@angular/core';
import cities        from '@my-weather-app/assets/cities/cities-fr.json';
import { CityModel } from '@my-weather-app/shared/model/city.model';

/**
 * Composant parent de l'application
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  /**
   * Villes de France
   */
  cities: Array<CityModel>;

  /**
   * Ville courante
   */
  currentCity: CityModel;

  /**
   * Instancie le composant principal de l'application
   */
  constructor() {
    this.cities = cities.map(city => new CityModel(city));
    this.currentCity = this.cities[0];
  }

}
