import { OverlayModule }             from '@angular/cdk/overlay';
import { CommonModule }              from '@angular/common';
import { ErrorHandler, NgModule }    from '@angular/core';
import { GlobalErrorHandlerService } from '@my-weather-app/core/service/global-error-handler.service';
import { OverlayService }            from '@my-weather-app/core/service/overlay.service';

/**
 * Module Core
 * Contient les services utilisés pour instancier les focntionnalités essentielles de l'application
 */
@NgModule({
  imports: [
    OverlayModule,
    CommonModule
  ],
  providers: [
    GlobalErrorHandlerService,
    OverlayService,
    {provide: ErrorHandler, useClass: GlobalErrorHandlerService}
  ]
})
export class CoreModule {
}
