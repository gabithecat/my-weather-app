import { Overlay, OverlayConfig, OverlayRef, PositionStrategy } from '@angular/cdk/overlay';
import { TemplatePortal }                                       from '@angular/cdk/portal';
import { Injectable, TemplateRef, ViewContainerRef }            from '@angular/core';

/**
 * Service du spinner de l'application
 *
 * NOTE: LE CODE PROVIENT D'UN TUTO (avec quelques ajustements)
 *
 */
@Injectable({
  providedIn: 'root'
})
export class OverlayService {
  constructor(private overlay: Overlay) {
  }

  static attachTemplatePortal(overlayRef: OverlayRef, templateRef: TemplateRef<any>, vcRef: ViewContainerRef) {
    const templatePortal = new TemplatePortal(templateRef, vcRef);
    overlayRef.attach(templatePortal);
  }

  createOverlay(config: OverlayConfig): OverlayRef {
    return this.overlay.create(config);
  }

  positionGloballyCenter(): PositionStrategy {
    return this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();
  }
}
