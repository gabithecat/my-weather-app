import { registerLocaleData } from '@angular/common';
import { HttpClientModule }   from '@angular/common/http';
import localFr                from '@angular/common/locales/fr';
import {
  LOCALE_ID,
  NgModule
}                             from '@angular/core';
import { FormsModule }        from '@angular/forms';
import {
  MatCardModule,
  MatDividerModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatTableModule,
  MatToolbarModule
}                             from '@angular/material';
import { BrowserModule }      from '@angular/platform-browser';

import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import { AppRoutingModule }         from '@my-weather-app/app-routing.module';
import { AppComponent }             from '@my-weather-app/app.component';
import { CoreModule }               from '@my-weather-app/core/core.module';
import { DetailWeatherComponent }   from '@my-weather-app/detail-weather/detail-weather.component';
import { ProgressSpinnerComponent } from '@my-weather-app/shared/component/progress-spinner/progress-spinner.component';
import { OpenWeatherService }       from '@my-weather-app/shared/service/open-weather.service';

registerLocaleData(localFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    DetailWeatherComponent,
    ProgressSpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatTableModule,
    MatToolbarModule,
    MatCardModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    FormsModule,
    HttpClientModule,
    CoreModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'fr'},
    OpenWeatherService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
