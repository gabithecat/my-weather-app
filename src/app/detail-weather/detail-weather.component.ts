import { Component, Input }           from '@angular/core';
import { MyWeatherAppErrorEnum }      from '@my-weather-app/shared/enum/my-weather-app-error.enum';
import { CityModel }                  from '@my-weather-app/shared/model/city.model';
import { CurrentWeatherModel }        from '@my-weather-app/shared/model/current-weather.model';
import { ForecastWeatherHourlyModel } from '@my-weather-app/shared/model/forecast-weather-hourly.model';
import { ForecastWeatherModel }       from '@my-weather-app/shared/model/forecast-weather.model';
import { OpenWeatherService }         from '@my-weather-app/shared/service/open-weather.service';
import * as moment                    from 'moment';

/**
 * Composant qui se charge de récupérer les données du détail de la météo d'une ville et de les afficher
 */
@Component({
  selector: 'app-detail-weather',
  templateUrl: './detail-weather.component.html',
  styleUrls: ['./detail-weather.component.scss']
})
export class DetailWeatherComponent {

  private pCity: CityModel;

  get city(): CityModel {
    return this.pCity;
  }

  /**
   * Ville courante
   * Paramètre d'entrée du composant
   *
   * Sur le changement de la valeur, de la ville courante, on met à jour la météo actuelle et les prévisions
   *
   * @param value ville courante
   */
  @Input()
  set city(value: CityModel) {
    this.pCity = value;
    this.displayProgressSpinner = true;
    this.searchCurrentWeatherData(this.pCity.id);
    this.searchForecastWeatherData(this.pCity.id);
  }

  /**
   * Données de la météo actuelle de la ville courante
   */
  currentWeather: CurrentWeatherModel;

  /**
   * Données des prévisions météo horaires de la ville courante
   */
  forecastWeather: ForecastWeatherModel;

  /**
   * Données du tableau des prévisions (table angular material)
   */
  dataSourceTableForecastWeather: Array<ForecastWeatherHourlyModel>;

  /**
   * Référence des colonnes (table angular material)
   */
  displayedColumns: Array<string> = ['jour', 'icon', 'tempMin', 'tempMax'];

  /**
   * Indicateur qui informe de la fin du traitement de récupération de la météo actuelle (succès ou erreur)
   */
  currentWeatherCompleted = false;

  /**
   * Indicateur qui informe de la fin du traitement de récupération des prévision météo horaires (succès ou erreur)
   */
  forecastWeatherCompleted = false;

  /**
   * Couleur du spinner
   */
  colorSpinner = 'primary';

  /**
   * Mode du spinner
   */
  modeSpinner = 'indeterminate';

  /**
   *
   */
  value = 50;

  /**
   * Flag d'affichage du spinner
   */
  displayProgressSpinner = false;

  /**
   * Instancie le composant d'affichage du détail de la météo d'une ville
   * @param openWeatherService service de l'Api open-weather
   */
  constructor(private openWeatherService: OpenWeatherService) {
  }

  /**
   * Récupère les données de la météo actuelle d'une ville
   * @param cityId identifiant de la ville
   */
  private searchCurrentWeatherData(cityId: number = 0): any {
    this.currentWeatherCompleted = false;

    // J'ai positionné un Timeout pour simuler une requête qui prend du temps afin de montrer le spinner
    setTimeout(() => {
      this.openWeatherService.getCurrentWeatherDataByCityId(cityId).subscribe(
        response => {
          this.currentWeather = response;
        },
        error => {
          throw new Error(MyWeatherAppErrorEnum.ECHEC_GET_CURRENT_WEATHER_BY_CITY_ID(cityId));
        },
        () => {
          // Gestion d'affichage du spinner
          this.currentWeatherCompleted = true;
          this.setDisplayProgressSpinner(this.currentWeatherCompleted, this.forecastWeatherCompleted);
        });
    }, 3000);
  }

  /**
   * Récupère les données des prévisions météo horaires sur 5 jours d'une ville
   * @param cityId identifiant de la ville
   */
  private searchForecastWeatherData(cityId: number = 0): any {
    this.forecastWeatherCompleted = false;

    // J'ai positionné un Timeout pour simuler une requête qui prend du temps afin de montrer le spinner
    setTimeout(() => {
    this.openWeatherService.getCurrentWeatherForecastDataByCityId(cityId).subscribe(
      response => {
        this.forecastWeather = response;
        this.dataSourceTableForecastWeather = this.forecastWeather.list;
      },
      error => {
        throw new Error(MyWeatherAppErrorEnum.ECHEC_GET_FORECAST_WEATHER_BY_CITY_ID(cityId));
      },
      () => {
        // Gestion d'affichage du spinner
        this.forecastWeatherCompleted = true;
        this.setDisplayProgressSpinner(this.currentWeatherCompleted, this.forecastWeatherCompleted);
      });
    }, 3000);
  }

  /**
   * Founit les classes css pour afficher l'icone correspondant à une météo
   */
  getCurrentWeatherIconClasses(): string {
    return (this.currentWeather
      && this.currentWeather.weather
      && this.currentWeather.weather[0]
      && this.currentWeather.weather[0].id
      && (`wi large-icon wi-icon-${this.currentWeather.weather[0].id}`)) || '';
  }

  /**
   * Founit les classes css pour afficher l'icone correspondant à une météo
   * @param forecastWeatherHourly prévision météo horaire
   */
  getForecastWeatherIconClasses(forecastWeatherHourly: ForecastWeatherHourlyModel): string {
    return (forecastWeatherHourly
      && forecastWeatherHourly.weather
      && forecastWeatherHourly.weather[0]
      && forecastWeatherHourly.weather[0].id
      && (`wi normal-icon wi-icon-${forecastWeatherHourly.weather[0].id}`)) || '';
  }

  /**
   * Extrait la température de la météo actuelle
   */
  getCurrentWeatherTemp(): number {
    return (this.currentWeather && this.currentWeather.main && this.currentWeather.main.temp) || 0;
  }

  /**
   * Extrait la date et l'heure d'une prévision météo horaire
   * @param forecastWeatherHourly prévision météo horaire
   */
  getForecastWeatherDate(forecastWeatherHourly: ForecastWeatherHourlyModel): Date | '' {
    return (forecastWeatherHourly && forecastWeatherHourly.dt && moment.unix(forecastWeatherHourly.dt).toDate()) || '';
  }

  /**
   * Extrait la température minimum d'une prévision météo horaire
   * @param forecastWeatherHourly prévision météo horaire
   */
  getForecastWeatherTempMin(forecastWeatherHourly: ForecastWeatherHourlyModel): number {
    return (forecastWeatherHourly && forecastWeatherHourly.main && forecastWeatherHourly.main.temp_min) || 0;
  }

  /**
   * Extrait la température maximum d'une prévision météo horaire
   * @param forecastWeatherHourly prévision météo horaire
   */
  getForecastWeatherTempMax(forecastWeatherHourly: ForecastWeatherHourlyModel): number {
    return (forecastWeatherHourly && forecastWeatherHourly.main && forecastWeatherHourly.main.temp_max) || 0;
  }

  /**
   * Met à jour l'état d'affichage du spinner en fonction de :
   * - l'état de récupération des données de la météo actuelle et des données
   * - l'état de récupération des données des prévision météo horaires
   * @param currentWeatherDownload état de récupération des données de la météo actuelle et des données
   *  (vrai : donnéees récupérées, false: données non téléchargées)
   * @param forecastWeatherDonwload état de récupération des données des prévision météo horaires
   *  (vrai : donnéees récupérées, false: données non téléchargées)
   */
  private setDisplayProgressSpinner(currentWeatherDownload: boolean, forecastWeatherDonwload: boolean) {
    this.displayProgressSpinner = !currentWeatherDownload || !forecastWeatherDonwload;
  }


}
