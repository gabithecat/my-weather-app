/**
 * Enumérateur sous forme de class qui énumère les messages d'erreur de l'application
 */
export class MyWeatherAppErrorEnum {

  public static ECHEC_GET_CURRENT_WEATHER_BY_CITY_ID = ((cityId: number): string =>
    `Echec lors de la récupération de la météo actuel (id ville : ${cityId})`);

  public static ECHEC_GET_FORECAST_WEATHER_BY_CITY_ID = ((cityId: number): string =>
    `Echec lors de la récupération des prévisions météo horaires (id ville : ${cityId})`);
}
