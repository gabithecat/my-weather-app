import { HttpClient }           from '@angular/common/http';
import { Injectable }           from '@angular/core';
import { CurrentWeatherModel }  from '@my-weather-app/shared/model/current-weather.model';
import { ForecastWeatherModel } from '@my-weather-app/shared/model/forecast-weather.model';
import { Observable }           from 'rxjs';

/**
 * Service qui permet d'utiliser l'API "Open Weather" via différents endpoints
 */
@Injectable({
  providedIn: 'root'
})
export class OpenWeatherService {

  private static readonly API_KEY = '72ed92bf46aedfbc6aa4eb3ad21dc8e4';

  /**
   * Instancie un service pour solliciter l'API Open Weather
   * @param http service HTTP
   */
  constructor(private http: HttpClient) { }

  /**
   * Service qui appelle l'API "Open Weather" qui récupère la météo actuelle d'une ville
   * @param cityId identifiant de la ville à filtrer
   */
  getCurrentWeatherDataByCityId(cityId: number): Observable<CurrentWeatherModel> {
    return this
      .http
      .get<CurrentWeatherModel>(
        `http://api.openweathermap.org/data/2.5/weather?id=${cityId}&units=metric&APPID=${OpenWeatherService.API_KEY}`);
  }

  /**
   * Service qui appele l'API "Open Weather" qui récupère les prévisions météo horaires d'une ville
   * (prévisions toutes les 3 heures pendant 5 jours).
   * @param cityId identifiant de la ville à filtrer
   */
  getCurrentWeatherForecastDataByCityId(cityId: number): Observable<ForecastWeatherModel> {
    return this
      .http
      .get<ForecastWeatherModel>(
        `http://api.openweathermap.org/data/2.5/forecast?id=${cityId}&units=metric&APPID=${OpenWeatherService.API_KEY}`);
  }

}
