/**
 * Interface qui spécifie la structure de la réponse de l'API "Open Weather" qui récupère la météo courante d'un ville
 */
export interface CurrentWeatherModel {

  id: number;

  name: string;

  cod: number;

  coord: {
    lon: number,
    lat: number
  };

  weather: Array<{
    id: number,
    main: string,
    description: string,
    icon: string
  }>;

  base: string;

  main: {
    temp: number,
    pressure: number,
    humidity: number,
    temp_min: number,
    temp_max: number,
    sea_level?: number,
    grnd_level?: number
  };

  visibility: number;

  wind: {
    speed: number,
    deg: number
  };

  clouds: {
    all: number;
  };

  rain: {
    '1h': number,
    '3h': number
  };

  snow: {
    '1h': number,
    '3h': number
  };

  dt: number;

  sys: {
    id: number,
    type: number,
    message: string,
    country: string,
    sunrise: number,
    sunset: number
  };

  timezone: number;


}
