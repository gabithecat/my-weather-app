import { ForecastWeatherHourlyModel } from '@my-weather-app/shared/model/forecast-weather-hourly.model';

/**
 * Interface qui spécifie la structure de la réponse de l'API "Open Weather" quirécupère les prévisions
 * météo horaires sur 5 jours d'une ville
 */
export interface ForecastWeatherModel {

  /**
   * Code général de la prévision
   */
  cod: number;

  /**
   * Message global de la prévision
   */
  message: string;

  /**
   * Ville pour laquelle ces prévisions sont destinées
   */
  city: {
    id: number,
    name: string,
    coord: {
      lon: number,
      lat: number
    },
    country: string,
    timezone: number
  };

  /**
   * Liste de prévisions météo horaire
   */
  list: Array<ForecastWeatherHourlyModel>;

  /**
   * Nombre de prévisions retournées par l'API
   */
  cnt: number;
}
