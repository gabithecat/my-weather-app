/**
 * Interface qui spécifie la structure d'une prévision météo horaire
 */
export interface ForecastWeatherHourlyModel {

  /**
   *
   */
  dt: number;

  /**
   *
   */
  main: {
    temp: number,
    pressure: number,
    humidity: number,
    temp_min: number,
    temp_max: number,
    sea_level: number,
    grnd_level: number,
    temp_kf: number
  };

  /**
   *
   */
  weather: Array<{
    id: number,
    main: string,
    description: string,
    icon: string
  }>;

  /**
   *
   */
  clouds: {
    all: number;
  };

  /**
   *
   */
  wind: {
    speed: number,
    deg: number
  };

  /**
   *
   */
  rain: {
    '1h': number,
    '3h': number
  };

  /**
   *
   */
  snow: {
    '1h': number,
    '3h': number
  };

  /**
   *
   */
  dt_txt: number;
}
