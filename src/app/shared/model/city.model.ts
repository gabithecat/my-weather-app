/**
 * Classe qui représente les données d'une ville
 */
export class CityModel {
  /**
   * Identifiant de la ville
   */
  id: number;

  /**
   * nom de la ville
   */
  name: string;

  /**
   * Coordonnée latitude
   */
  latitude: number;

  /**
   * Coordonnée longitude
   */
  longitude: number;

  /**
   * Instancie une nouvelle valeur d'une ville
   * @param other objet à copier
   */
  constructor(other: any = {}) {
    ({id: this.id, nm: this.name, lat: this.latitude, lon: this.longitude} = other);
  }
}
